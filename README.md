# Demo project specs

## About

A proposition project devops specs.   
- easy to understand for developers  
- easy to parse and automate for sysadmins.

The main definition is in 'project.yml' in the root directory. The overrides directory contains files with certain config overrides, also defined in the project.yml file. 

Because these are in a repo, they can follow a certain (automated) review process, before allowing the deploy to be executed.

## Benefits

- Developers can easily define their needs.
- Because of the abstraction, devs don't need to know kubernetes.
- Sysadmins have less work maintaining projects.
- Spec definition changes can trigger a review process for sysadmins.
- Easy to look up current situation.

## Hooks
A list of yml files that can be used to define commands needing to executed during the deloyment, on different phases during the deployment.
These yaml files are using the ansible language and based on the [ ansistrano deployment role](https://github.com/ansistrano/deploy).
See the Ansistrano flow at https://github.com/ansistrano/deploy/blob/master/docs/ansistrano-flow.png

